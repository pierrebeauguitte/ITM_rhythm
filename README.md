Code accompanying the article *Rhythm inference from audio recordings of Irish traditional music* (Beauguitte P., Duggan B., and Kelleher J. D., [Folk Music Analysis 2018](http://fma2018.mus.auth.gr/))

### Python dependencies:

* Python 2.7
* numpy
* scipy
* sklearn
* matplotlib
* madmom

### Instructions:
to create the dataset, run

```
python makeDataset.py --path <path> --output <output> --ext <ext>
```
where `<path>` is the directory containing the audio files, `<output>` is the destination directory for extracted features (default: `<path>`), and `<ext>` is the audio file extension (default: mp3).

to run the experiment, run

```
python train.py --path <path> --reference <ref> --modelType <model>
```
where `<path>` is the directory containing the extracted features, `<ref>` is a csv file containing the reference labels, and `<model>` is either `binomial` (exp. A) or `multinomial` (exp. B).

### Notes:
The audio collection used in the article is the set of recordings published by Comhaltas Ceoltóirí Éireann on the Foinn Seisiún CDs (https://comhaltas.ie/).

In the file `reference.csv` there is no tune at index 218: we discarded it because it is a duplicate of recording index 114.

The results reported in the paper are reproducible, thanks to the seeding of the numpy random number generator in the main function of `train.py`. In addition, to reproduce the results of exp. A, one line needs to be uncommented in the `makeSplits` function (this is a consequence of the refactoring of the code after publication ; it only affects the splitting of the dataset for *k*-fold cross validation).
