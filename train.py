import matplotlib.pyplot as plt
import numpy as np
import glob
import csv
import json
import sys
import argparse
import os
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix

def cleanFilename(filename):
    return os.path.splitext(os.path.basename(filename))[0]

def loadReference(referenceFile, modelType, labels):

    tunes = {}
    perID = {}

    # mapping from tune type to meter type for binomial model
    types = {
        'jig':       'compound',
        'polka':     'simple',
        'reel':      'simple',
        'slide':     'compound',
        'mazurka':   'simple',
        'hornpipe':  'simple',
        'waltz':     'simple',
        'slip jig':  'compound',
        'barndance': 'simple',
        'song':      'simple',
        'fling':     'simple'
    }

    with open(referenceFile, 'r') as ref:
        reader = csv.reader(ref)
        for row in reader:
            if modelType == 'multinomial' and row[1] not in labels:
                continue
            tuneName = cleanFilename(row[0])
            tuneType = row[1] if modelType == 'multinomial' \
                       else types[row[1]]
            if tuneType not in tunes:
                tunes[tuneType] = []
            tunes[tuneType].append(tuneName)
            perID[tuneName] = tuneType

    return tunes, perID

def prepareSets(path, tuneDict):

    x = [] # quantized peaks
    y = [] # label
    t = [] # tune index
    minNWindows = None

    dataFiles = glob.glob('%s/*.json' % path)
    for tune in dataFiles:
        tuneName = cleanFilename(tune)
        if tuneName not in tuneDict:
            continue
        target = tuneDict[tuneName]
        fp = open(tune, 'r')
        data = json.load(fp)
        ts = map(int, data.keys())
        ts.sort()
        if len(ts) < minNWindows or minNWindows is None:
            minNWindows = len(ts)
        for a in ts:
            x.append(data[str(a)])
            y.append(target)
            t.append(tuneName)

    return x, y, t, minNWindows

def makeSplits(instances, nFolds):
    splits = {i:[] for i in instances}
    for tuneType in instances:
        tuneIds = np.array(instances[tuneType])
        shuffle = np.random.permutation(len(tuneIds))
        splitSize = len(tuneIds) / float(nFolds)
        # uncomment the following line to reproduce results of Exp. A
        # splitSize = round(splitSize)
        for i in range(nFolds - 1):
            splits[tuneType].append(
                list(tuneIds[ shuffle[int(np.round(i*splitSize)) :
                                      int(np.round((i+1)*splitSize))] ])
            )
        splits[tuneType].append(
            list(tuneIds[ shuffle[int(np.round((nFolds-1)*splitSize)):] ])
        )
    return splits

def getScores(fy, fp, ft, wLen):
    tindices = np.array(ft)
    indices = set(ft)
    globalRes = []
    for i in indices:
        loc = np.where(tindices == i)[0]
        res = []
        for w in range(len(loc) - wLen + 1):
            score = 0
            for t in range(wLen):
                if (fy[loc[w+t]] == fp[loc[w+t]]):
                    score += 1
            score /= float(wLen)
            res.append( 1 if (score > 0.5) else 0 )
        globalRes.extend(res)
    return globalRes

def prepareTrainAndTest(path, referenceFile, modelType):

    nFolds = 4 if modelType == 'multinomial' else 10

    if modelType == 'binomial':
        labels = ['simple', 'compound']
    else:
        labels = ['reel', 'jig', 'polka', 'hornpipe', 'slide']

    t, ref = loadReference(referenceFile, modelType, labels)
    X, Y, names, minN = prepareSets(path, ref)
    splits = makeSplits(t, nFolds)

    print 'Starting cross-validation'

    wLens = range(1, minN if minN % 2 == 0 else minN + 1, 2)
    finalRes = {s: [] for s in wLens}

    confMat = np.zeros((len(labels),len(labels)), dtype = int)
    tuneConfMat = np.zeros((len(labels),len(labels)), dtype = int)
    tuneErrors = {}

    for i in range(nFolds):
        print 'Fold %d' % i
        testId = [ s[i] for s in splits.values() ]
        testId = [ x for s in testId for x in s ] # flatten list of lists

        train_x = []
        train_y = []
        test_x = []
        test_y = []
        test_t = []
        for j in range(len(X)):
            if names[j] in testId:
                test_x.append(X[j])
                test_y.append(Y[j])
                test_t.append(names[j])
            else:
                train_x.append(X[j])
                train_y.append(Y[j])

        logreg = LogisticRegression(class_weight = 'balanced')
        logreg.fit(train_x, train_y)

        score = logreg.score(test_x, test_y)
        print 'Accuracy on test set: %.3f' % score

        prediction = logreg.predict(test_x)
        cm = confusion_matrix(test_y, prediction,
                              labels = labels)
        confMat += cm

        for r in finalRes:
            finalRes[r].extend(getScores(test_y, prediction, test_t, r))

        tune_prediction = {}
        for p in range(len(test_x)):
            tune_id = test_t[p]
            if tune_id not in tune_prediction:
                tune_prediction[tune_id] = dict.fromkeys(labels, 0)
            tune_prediction[tune_id][prediction[p]] += 1
        for k in tune_prediction:
            p = max(tune_prediction[k].iteritems(), key = lambda x: x[1])[0]
            index_p = labels.index(p)
            index_ref = labels.index(ref[k])
            tuneConfMat[ index_ref, index_p ] += 1
            if index_p != index_ref:
                tuneErrors[k] = p

    print '\nAggregate confusion matrix (frame):'
    print confMat
    print 'Frame acccuracy: %.2f%%' % (100. * np.trace(confMat) / np.sum(confMat))

    print '\nAggregate confusion matrix (tune):'
    print tuneConfMat
    print 'Tune acccuracy: %.2f%%' % (100. * np.trace(tuneConfMat) / np.sum(tuneConfMat))

    print '\nWrong predictions on tunes:'
    for te in tuneErrors:
        print '\t%s (%s), recognised as %s' % (te, ref[te], tuneErrors[te])

    plotValues = []
    for r in wLens:
        plotValues.append(100. * sum(finalRes[r]) / float(len(finalRes[r])))

    plt.plot(wLens, plotValues)
    plt.show()

    return wLens, plotValues

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--path', required=True,
                        help='the path for json files')
    parser.add_argument('--reference', required=True,
                        help='reference csv file')
    parser.add_argument('--modelType', required=True,
                        help='type of model to train (binomial/multinomial)')
    args = parser.parse_args()

    if not os.path.exists(args.path):
        print 'path does not exist, aborting'
        sys.exit(1)

    if not os.path.isfile(args.reference):
        print 'reference does not exist, aborting'
        sys.exit(1)

    if args.modelType == 'binomial':
        np.random.seed(12345)
    elif args.modelType == 'multinomial':
        np.random.seed(1678)
    else:
        print 'modelType should be \'binomial\' or \'multinomial\''
        sys.exit(1)

    plotX, plotY = prepareTrainAndTest(args.path.rstrip('/'),
                                       args.reference,
                                       args.modelType)
